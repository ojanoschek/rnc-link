// ==UserScript==
// @name		RNC+ Permanent Link
// @author		Rapante
// @version		0.4
// @description Adds a permanent link to the Random Nonsense Club
// @match		https://forums.oneplus.net/*
// @copyright	2012+, Rapante
// @require		http://code.jquery.com/jquery-latest.js
// @namespace https://greasyfork.org/users/3232
// ==/UserScript==

/*jslint browser: true*/
/*global $, jQuery, alert*/

$(document).ready(function()
{
    $( ".nav-pills" ).append( "<li><a href='https://forums.oneplus.net/threads/offtopic-random-nonsense-club.358/unread' style='width:44px;left:-400px;background-image:url(https://bytebucket.org/ojanoschek/rnc-link/raw/e0305a90e22672e0bb7ed24d91fea3a722ba2ab5/i/logo_light.gif);text-indent:-9999px;'>RNC</a></li>" );
    //$( ".nav-pills" ).append( "<li><a href='https://forums.oneplus.net/threads/offtopic-random-nonsense-club.358/unread' style='width:44px;left:-400px;background-image:url(https://bytebucket.org/ojanoschek/rnc-link/raw/e0305a90e22672e0bb7ed24d91fea3a722ba2ab5/i/logo_dark.gif);text-indent:-9999px;'>RNC</a></li>" );
});